use std::path::PathBuf;

#[derive(Debug, Default, Clone)]
pub struct Sheet {
    pub path: PathBuf,
    pub frames: Vec<SheetFrame>,
    pub frame_slow_factor: usize,
    pub is_loop: bool,
}

#[derive(Debug, Default, Clone)]
pub struct SheetFrame {
    pub x: usize,
    pub y: usize,
    pub width: usize,
    pub height: usize,
}

impl Sheet {
    pub fn new(
        path: PathBuf,
        frames: Vec<SheetFrame>,
        frame_slow_factor: usize,
        is_loop: bool,
    ) -> Self {
        Self {
            path,
            frames,
            frame_slow_factor,
            is_loop,
        }
    }
}

impl SheetFrame {
    pub fn new(x: usize, y: usize, width: usize, height: usize) -> Self {
        Self {
            x,
            y,
            width,
            height,
        }
    }
}

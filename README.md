# OPP Playground

A playground project to test animations using the [ggez](http://ggez.rs/) library.

Assets come from the [Open Pixel Project](http://www.openpixelproject.com/)

# End goal

Ideally this would end up as a playable game or at least a base to build a platformer or something similar.

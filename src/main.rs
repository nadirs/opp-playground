extern crate ggez;

mod config;
mod sheet;
mod state;

use std::env;
use std::path;

use self::state::MainState;

use ggez::*;

fn main() {
    let config = config::Config::new();

    let cb = ContextBuilder::new("opp_playground", "ggez")
        .window_setup(conf::WindowSetup::default().title("OPP Playground"))
        .window_mode(
            conf::WindowMode::default().dimensions(config.window_width, config.window_height),
        );
    let ctx = &mut cb.build().unwrap();

    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("assets");
        ctx.filesystem.mount(&path, true);
    }

    //ctx.filesystem.mount(&config.assets_path, true);

    let state = &mut MainState::init(ctx, config).unwrap();
    event::run(ctx, state).unwrap();
}

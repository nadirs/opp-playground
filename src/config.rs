use std::collections::HashMap;
use std::iter::FromIterator;
use std::path::PathBuf;

use super::sheet::Sheet;
use super::sheet::SheetFrame;

pub type SheetMap = HashMap<SheetKey, Sheet>;

#[derive(Debug, Default)]
pub struct Config {
    pub assets_path: PathBuf,
    pub sheets: SheetMap,
    pub window_width: u32,
    pub window_height: u32,
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum SheetKey {
    TravelerIdle,
    TravelerWalk,
    TravelerRun,
    TravelerDuck,
    TravelerSlide,
}

impl Config {
    pub fn new() -> Self {
        Self {
            assets_path: Self::_init_assets_path(),
            sheets: Self::_init_sheets(),
            window_width: 600,
            window_height: 400,
        }
    }

    fn _init_assets_path() -> PathBuf {
        PathBuf::from_iter(vec![env!("CARGO_MANIFEST_DIR"), "assets"])
    }

    fn _init_sheets() -> SheetMap {
        HashMap::from_iter(vec![
            (
                SheetKey::TravelerIdle,
                Sheet::new(
                    PathBuf::from("/spr_m_traveler_idle_anim.png".to_string()),
                    vec![
                        SheetFrame::new(64 * 0, 0, 64, 64),
                        SheetFrame::new(64 * 1, 0, 64, 64),
                        SheetFrame::new(64 * 2, 0, 64, 64),
                        SheetFrame::new(64 * 3, 0, 64, 64),
                        SheetFrame::new(64 * 4, 0, 64, 64),
                        SheetFrame::new(64 * 5, 0, 64, 64),
                        SheetFrame::new(64 * 6, 0, 64, 64),
                        SheetFrame::new(64 * 7, 0, 64, 64),
                    ],
                    6,
                    true,
                ),
            ),
            (
                SheetKey::TravelerWalk,
                Sheet::new(
                    PathBuf::from("/spr_m_traveler_walk_anim.png".to_string()),
                    vec![
                        SheetFrame::new(64 * 0, 0, 64, 64),
                        SheetFrame::new(64 * 1, 0, 64, 64),
                        SheetFrame::new(64 * 2, 0, 64, 64),
                        SheetFrame::new(64 * 3, 0, 64, 64),
                        SheetFrame::new(64 * 4, 0, 64, 64),
                        SheetFrame::new(64 * 5, 0, 64, 64),
                        SheetFrame::new(64 * 6, 0, 64, 64),
                        SheetFrame::new(64 * 7, 0, 64, 64),
                    ],
                    5,
                    true,
                ),
            ),
            (
                SheetKey::TravelerRun,
                Sheet::new(
                    PathBuf::from("/spr_m_traveler_run_anim.png".to_string()),
                    vec![
                        SheetFrame::new(64 * 0, 0, 64, 64),
                        SheetFrame::new(64 * 1, 0, 64, 64),
                        SheetFrame::new(64 * 2, 0, 64, 64),
                        SheetFrame::new(64 * 3, 0, 64, 64),
                        SheetFrame::new(64 * 4, 0, 64, 64),
                        SheetFrame::new(64 * 5, 0, 64, 64),
                    ],
                    5,
                    true,
                ),
            ),
            (
                SheetKey::TravelerDuck,
                Sheet::new(
                    PathBuf::from("/spr_m_traveler_duck_anim.png".to_string()),
                    vec![
                        SheetFrame::new(64 * 0, 0, 64, 64),
                        SheetFrame::new(64 * 1, 0, 64, 64),
                        SheetFrame::new(64 * 2, 0, 64, 64),
                        SheetFrame::new(64 * 3, 0, 64, 64),
                    ],
                    3,
                    false,
                ),
            ),
            (
                SheetKey::TravelerSlide,
                Sheet::new(
                    PathBuf::from("/spr_m_traveler_slide_anim.png".to_string()),
                    vec![
                        SheetFrame::new(64 * 0, 0, 64, 64),
                        SheetFrame::new(64 * 1, 0, 64, 64),
                    ],
                    3,
                    false,
                ),
            ),
        ])
    }
}

use std::collections::HashSet;
use std::iter::FromIterator;

use ggez::event::{Keycode, Mod};
use ggez::graphics::Point2;
use ggez::*;

use super::config::{Config, SheetKey};
use super::sheet::SheetFrame;

pub const SPEED_IDLE: f32 = 0.;
pub const SPEED_WALK: f32 = 2.;
pub const SPEED_RUN: f32 = 4.;

#[derive(Debug)]
pub enum HorizontalDirection {
    Left,
    Right,
}

impl Default for HorizontalDirection {
    fn default() -> Self {
        HorizontalDirection::Right
    }
}

#[derive(Debug)]
pub struct MainState {
    config: Config,
    player_x: f32,
    player_y: f32,
    current_frame: usize,
    player_sheet: SheetKey,
    player_direction: HorizontalDirection,
    key_state: HashSet<Keycode>,
}

impl MainState {
    pub fn init(ctx: &mut Context, config: Config) -> GameResult<Self> {
        let player_initial_y = config.window_height as f32 - 164.;
        ctx.print_resource_stats();
        Ok(Self {
            config: config,
            player_sheet: SheetKey::TravelerIdle,
            player_x: 20.,
            player_y: player_initial_y,
            player_direction: Default::default(),
            current_frame: 0,
            key_state: Default::default(),
        })
    }

    fn draw_player(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::set_color(ctx, graphics::Color::from_rgb(255, 255, 255))?;

        self.config
            .sheets
            .get(&self.player_sheet)
            .cloned()
            .map(|sheet| {
                if sheet.is_loop
                    || self.current_frame < ((sheet.frames.len() - 1) * sheet.frame_slow_factor)
                {
                    self.current_frame =
                        (self.current_frame + 1) % (sheet.frames.len() * sheet.frame_slow_factor)
                }
                let image: graphics::Image = graphics::Image::new(ctx, &sheet.path).unwrap();
                let counter: usize = self.current_frame / sheet.frame_slow_factor;
                let frame: Option<&SheetFrame> = sheet.frames.get(counter);
                frame.map(|f| graphics::draw_ex(ctx, &image, self.frame_draw_param(&image, f)))
                // make sure errors are raised
            });

        Ok(())
    }

    fn frame_draw_param(&self, image: &graphics::Image, f: &SheetFrame) -> graphics::DrawParam {
        let hscale = match self.player_direction {
            HorizontalDirection::Left => -1.0,
            HorizontalDirection::Right => 1.0,
        };
        let vscale = 1.0;

        graphics::DrawParam {
            src: graphics::Rect::new(
                f.x as f32 / image.width() as f32,
                f.y as f32 / image.height() as f32,
                f.width as f32 / image.width() as f32,
                f.height as f32 / image.height() as f32,
            ),
            dest: Point2::new(self.player_x, self.player_y + f.height as f32),
            offset: Point2::new(0.5, 0.),
            scale: Point2::new(hscale, vscale),
            ..graphics::DrawParam::default()
        }
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        let prev_animation = self.player_sheet.clone();
        let walk_keycodes = HashSet::from_iter(vec![Keycode::Left, Keycode::Right]);
        let is_ducking = self.key_state.contains(&Keycode::Down);
        let is_moving = !self.key_state.is_disjoint(&walk_keycodes);
        let is_running = self.key_state.contains(&Keycode::X);

        let mut speed = SPEED_IDLE;
        if is_ducking {
            if is_moving && is_running {
                self.player_sheet = SheetKey::TravelerSlide;
                speed = SPEED_RUN;
            } else {
                self.player_sheet = SheetKey::TravelerDuck;
            }
        } else if is_moving {
            if is_running {
                self.player_sheet = SheetKey::TravelerRun;
                speed = SPEED_RUN;
            } else {
                self.player_sheet = SheetKey::TravelerWalk;
                speed = SPEED_WALK;
            }
        } else {
            self.player_sheet = SheetKey::TravelerIdle;
        }

        if prev_animation != self.player_sheet {
            self.current_frame = 0;
        }

        if self.key_state.contains(&Keycode::Left) {
            self.player_direction = HorizontalDirection::Left;
            speed *= -1.;
        } else if self.key_state.contains(&Keycode::Right) {
            self.player_direction = HorizontalDirection::Right;
        }
        self.player_x += speed;

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        self.draw_player(ctx)?;
        graphics::present(ctx);

        Ok(())
    }
    fn key_down_event(&mut self, ctx: &mut Context, keycode: Keycode, _keymod: Mod, _repeat: bool) {
        if let Keycode::Escape = keycode {
            let _result = ctx.quit();
            _result.expect("Error on quit");
        }
        self.key_state.insert(keycode);
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: Keycode, _keymod: Mod, _repeat: bool) {
        self.key_state.remove(&keycode);
    }
}
